const CrearEmpleado = require("../../LogicaDeLaAplicacion/CasosDeUso/CrearEmpleado").CrearEmpleado;
const ListarEmpleados = require("../../LogicaDeLaAplicacion/CasosDeUso/ListarEmpleados").ListarEmpleados;


class EmpleadoControlador {
    crearEmpleado(empleado, repositorioEmpleado) {
        const crearEmpleado = new CrearEmpleado(repositorioEmpleado, empleado);
        crearEmpleado.crearNuevoEmpleado();

        return "OK";
    }

    listarTodosLosEmpleados(repositorioEmpleado){
        const listaEmpleados = new ListarEmpleados(repositorioEmpleado);
        return listaEmpleados.listaDeEmpleados();
    }
}

module.exports = { EmpleadoControlador };